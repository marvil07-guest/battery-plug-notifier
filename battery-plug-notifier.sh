#!/bin/sh

LOW_THRESHOLD=40
HIGH_THRESHOLD=80
SLEEP_TIME='10m'

NAME=`basename $0`
if [ ! -z "$DISPLAY" ]
then
    NOTIFY_COMMAND='notify-send'
else
    # For CLI, nothing.
    logger "$NAME: Not running without UI"
    exit 1
fi

get_ac_status() {
    ACPI_LINE="$1"
    logger "$NAME: get_ac_status, acpi line '$ACPI_LINE'"
    echo "$ACPI_LINE" | grep 'Charging' > /dev/null
    if [ $? -eq 0 ]
    then
        AC_STATUS='Charging'
    else
        AC_STATUS='Discharging'
    fi
    echo $AC_STATUS
}

detect_and_notify() {
    ACPI_LINE=`acpi --battery`
    logger "$NAME: detect_and_notify, acpi line '$ACPI_LINE'"
    BATTERY_PERCENTAGE=`echo "$ACPI_LINE" | sed 's/.*, \(.*\)%,.*/\1/'`
    logger "$NAME: detect_and_notify, battery % '$BATTERY_PERCENTAGE'"
    AC_STATUS=`get_ac_status "$ACPI_LINE"`
    logger "$NAME: detect_and_notify, ac status % '$AC_STATUS'"
    if [ "$BATTERY_PERCENTAGE" -lt "$LOW_THRESHOLD" -a "$AC_STATUS" = 'Discharging' ]
    then
        logger "$NAME: detect_and_notify, notyfying plug"
        SUMMARY='Plug AC'
        MESSAGE="Battery ${BATTERY_PERCENTAGE}%"
        $NOTIFY_COMMAND "$SUMMARY" "$MESSAGE"
    elif [ "$BATTERY_PERCENTAGE" -gt "$HIGH_THRESHOLD" -a "$AC_STATUS" = 'Charging' ]
    then
        logger "$NAME: detect_and_notify, notyfying unplug"
        SUMMARY="Unplug AC"
        MESSAGE="Battery ${BATTERY_PERCENTAGE}%"
        $NOTIFY_COMMAND "$SUMMARY" "$MESSAGE"
    else
        logger "$NAME: detect_and_notify, notyfying do nothing"
    fi
}

while true
do
    detect_and_notify
    sleep $SLEEP_TIME
done
